.PHONY: clean lesson_1

clean:
	rm --force --recursive color-buttom

lesson_1:
	npx create-react-app color-buttom

tests:
	cd color-buttom && npm test